from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from sqloperations.models import Snippets, Tag
from datetime import datetime
import pytz
from sqloperations.decorator.logindec import logindecorator

tz = pytz.timezone('US/Pacific')
datetime_1 = datetime.now(tz)


def createorget(tag):
    tagqs = Tag.objects.filter(title=tag, value =tag).first()
    if tagqs:
        return tagqs
    else:
        qs = Tag.objects.create(title=tag, value=tag)
        return qs


@api_view(['POST'])
@logindecorator
def Createsnip(request, user):
    text = request.data.get("text")
    title = request.data.get("title")
    tag = request.data.get("tag")
    datatime = str(datetime_1)
    print(text, title)
    try:
        qstag = createorget(tag)
        qs = Snippets.objects.create(user=user, title=title, text=text, timestamp=datatime, tag=qstag)
        context = {
            "result": "success",
            "code": 200
        }
        # print(context)
        return Response(context)
    except Exception as e:
        print(e)
        context = {
            "result": f"internal server error, {e}",
            "code": 200
        }
        return Response(context)

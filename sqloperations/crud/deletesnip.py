from rest_framework.response import Response
from rest_framework.decorators import api_view
from sqloperations.models import Snippets
from datetime import datetime
import pytz
from sqloperations.decorator.logindec import logindecorator

tz = pytz.timezone('US/Pacific')
datetime_1 = datetime.now(tz)


@api_view(['DELETE'])
@logindecorator
def Deletesnip(request, user):
    snipid = request.data.get("snipid")
    print(snipid)
    try:
        qs = Snippets.objects.get(user=user, id=snipid)
        qs.delete()
        context = {
            "result": "success",
            "code": 200
        }
        return Response(context)
    except Exception as e:
        print(e)
        context = {
            "result": f"internal server error, {e}",
            "code": 200
        }
        return Response(context)

from rest_framework.response import Response
from rest_framework.decorators import api_view
from sqloperations.models import Snippets
from rest_framework import serializers
from datetime import datetime
import pytz
from sqloperations.decorator.logindec import logindecorator
tz = pytz.timezone('US/Pacific')
datetime_1 = datetime.now(tz)


class SnipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Snippets
        fields = "__all__"


@api_view(['GET'])
@logindecorator
def SnipGetview(request, user):
    snipid = request.data.get("snipid")
    try:
        qs = Snippets.objects.filter(user=user)
        if qs:
            serializer = SnipSerializer(qs, many=True)
            context = {
                "result": serializer.data,
                "code": 200
            }
            # print(context)
            return Response(context , status=200)
        else:
            context = {
                "result": "error finding data",
                "code": 500
            }
            print(context)
            return Response(context, status=500)
    except Exception as e:
        print(e)
        context = {
            "result": f"error finding data,{e}",
            "code": 500
        }
        print(context)
        return Response(context)

from rest_framework.response import Response
from rest_framework.decorators import api_view
from sqloperations.models import Snippets
from datetime import datetime
import pytz
from sqloperations.decorator.logindec import logindecorator
from sqloperations.crud.createsnip import createorget
tz = pytz.timezone('US/Pacific')
datetime_1 = datetime.now(tz)


@api_view(['PATCH'])
@logindecorator
def Updatesnip(request, user):
    text = request.data.get("text")
    title = request.data.get("title")
    snipid = request.data.get("id")
    tag = request.data.get("tag")
    print(text, title, snipid, tag)
    datatime = str(datetime_1)
    try:
        qstag = createorget(tag)
        qs = Snippets.objects.get(id=snipid, user=user)
        qs.tag = qstag
        qs.title = title
        qs.text = text

        print(qs.text, "fgt")
        qs.save()
        context = {
            "result": "success",
            "code": 200
        }
        # print(context)
        return Response(context, status=200)
    except Exception as e:
        print(e)
        context = {
            "result": f"internal server error, {e}",
            "code": 500
        }
        print(context)
        return Response(context, status=500)

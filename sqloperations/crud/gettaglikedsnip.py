from rest_framework.response import Response
from rest_framework.decorators import api_view
from sqloperations.models import Snippets
from sqloperations.models import Tag
from sqloperations.decorator.logindec import logindecorator
from rest_framework import serializers


class SnipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Snippets
        fields = "__all__"


@api_view(['POST'])
@logindecorator
def GetTagSnip(request, user):
    title = request.data.get("title")
    print("hii", title)
    try:
        qstag = Tag.objects.get(title=title)
        if qstag:
            qs = Snippets.objects.filter(tag=qstag, user=user)
            serializer = SnipSerializer(qs, many=Tag)
            print(serializer.data)
            context = {
                "result": serializer.data,
                "code": 200
            }
            print(context)
            return Response(context, status=200)
        else:
            context = {
                "result": "Tag with snip not found",
                "code": 500
            }
            print(context)
            return Response(context, status=200)
    except Exception as e:
        print(e)
        context = {
            "result": f"internal server error, {e}",
            "code": 500
        }
        print(context)
        return Response(context, status=500)

from rest_framework.response import Response
from rest_framework.decorators import api_view
from sqloperations.models import Snippets
from sqloperations.models import Tag
from datetime import datetime
import pytz
from sqloperations.decorator.logindec import logindecorator
from rest_framework import serializers

tz = pytz.timezone('US/Pacific')
datetime_1 = datetime.now(tz)


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


@api_view(['GET'])
@logindecorator
def GetTag(request, user):
    try:
        qs = Tag.objects.all()
        serializer = TagSerializer(qs, many=Tag)
        print(serializer.data)
        context = {
            "result": serializer.data,
            "code": 200
        }
        return Response(context, status=200)
    except Exception as e:
        print(e)
        context = {
            "result": f"internal server error, {e}",
            "code": 500
        }
        print(context)
        return Response(context, status=500)

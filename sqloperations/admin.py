from django.contrib import admin
from .models import Snippets, Tag
# Register your models here.
admin.site.register(Snippets)
admin.site.register(Tag)

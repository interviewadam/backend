from django.http import HttpResponse, JsonResponse
import json
from user.models import User
import jwt


def logindecorator(fun):
    def wrap(request):
        try:
            token = request.headers.get("Authorization")
            # print(request.headers.get("Authorization"), "bhbhhb")
            usertoken = jwt.decode(token, "secret", algorithms=["HS256"])
            useremail  = usertoken.get('username')
            auth = User.objects.filter(email=useremail).first()
            if auth:
                hi = fun(request, auth)
                print(hi)
                return hi
            else:
                print("else")
                context = {
                    "result": "user authentication failed"
                }
                return HttpResponse(json.dumps(context), content_type="application/json")
        except jwt.exceptions.InvalidSignatureError:
            context = {
                "result": "invalid user token"
            }
            return HttpResponse(json.dumps(context), content_type="application/json")
    return wrap

from django.db import models
from user.models import User


# Create your models here.
class Tag(models.Model):
    title = models.CharField(max_length=256, unique=True)
    value = models.CharField(max_length=256)

class Snippets(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.DO_NOTHING, null=True, blank=True)
    title = models.CharField(max_length=256)
    text  = models.TextField()
    timestamp = models.DateTimeField()

from django.apps import AppConfig


class SqloperationsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sqloperations'

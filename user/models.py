from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

# Create your models here.
class User(models.Model):
    email = models.EmailField(unique=True)
    staff = models.BooleanField(default=False)
    customer = models.BooleanField(default=True)
    admin = models.BooleanField(default=False)

    def __str__(self):
        return self.email

class Otp(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    otp  =  models.IntegerField()

    def __str__(self):
        return f"{self.user.email} otp {self.otp}"

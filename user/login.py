

from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import User, Otp
import jwt


def otpcheck(qs, otp):
    otpcheckuser = Otp.objects.filter(user=qs).first()
    if otpcheckuser:
        return True
    else:
        return False


def endcodetoken(user):
    try:
        encoded = jwt.encode({"username": user}, "secret", algorithm="HS256")
        return encoded
    except Exception as e:
        print(e)
        return "error"


@api_view(['POST'])
def Login(request):
    email = request.data.get("email")
    otp = request.data.get("otp")
    qs = User.objects.filter(email=email).first()
    if qs:
        checkotp = otpcheck(qs, otp)
        if checkotp:
            token = endcodetoken(qs.email)
            request.COOKIES['token'] = token
            context = {
                "result": "sucess",
                "token": token,
                "code": 200
            }
            print(context)
            return Response(context, status=200)
        else:
            context = {
                "result": "failure otp does not match",
                "code": 500
            }
            return Response(context, status=500)
    else:
        context = {
            "result": "email id is not registered in our database",
            "code": 500
        }
        return Response(context)

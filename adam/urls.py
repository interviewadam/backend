from django.contrib import admin
from django.urls import path
from user.sendotpemail import Emailotp
from user.login import Login
from sqloperations.crud.createsnip import Createsnip
from sqloperations.crud.getsnip import SnipGetview
from sqloperations.crud.deletesnip import Deletesnip
from sqloperations.crud.getsnipdetailview import Detailview
from sqloperations.crud.updatesnip import Updatesnip
from sqloperations.crud.gettags import GetTag
from sqloperations.simplytry import Simpletry
from sqloperations.crud.gettaglikedsnip import GetTagSnip


urlpatterns = [
    path('admin/', admin.site.urls),
    path('otp/', Emailotp),
    path('login/',Login),
    path('createsnip/', Createsnip),
    path('getsnip/', SnipGetview),
    path('deletesnip/', Deletesnip),
    path('detailview/', Detailview),
    path('updatesnip/', Updatesnip),
    path('gettags/', GetTag),
    path('try/', Simpletry),
    path('gettaggedsnip/', GetTagSnip)
]
